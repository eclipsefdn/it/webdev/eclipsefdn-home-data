<?php

require_once "/home/data/httpd/eclipse-php-classes/system/dbconnection.class.php";

/**
 * Project.class.php.
 *
 * Author:   Denis Roy
 * Date:    2004-11-16
 *
 * Description: Functions and modules related to the MySQL database connection
 *
 * HISTORY:
 */
class Project {

  public $project_id = "";
  public $name = "";
  public $level = 0;
  public $parent_project_id = "";
  public $description = "";
  public $url_download = "";
  public $url_index = "";
  public $is_topframe = 0;
  public $drop;

  public function getProjectID() {
    return $this->project_id;
  }

  public function getName() {
    return $this->name;
  }

  public function getlevel() {
    return $this->level;
  }

  public function getParentProjectID() {
    return $this->parent_project_id;
  }

  public function getDescription() {
    return $this->description;
  }

  public function getUrlDownload() {
    return $this->url_download;
  }

  public function getUrlIndex() {
    return $this->url_index;
  }

  public function getIsTopFrame() {
    return $this->is_topframe;
  }

  public function getDrop() {
    return $this->drop;
  }

  public function setProjectID($_project_id) {
    $this->project_id = $_project_id;
  }

  public function setName($_name) {
    $this->name = $_name;
  }

  public function setlevel($_level) {
    $this->level = $_level;
  }

  public function setParentProjectID($_parent_project_id) {
    $this->parent_project_id = $_parent_project_id;
  }

  public function setDescription($_description) {
    $this->description = $_description;
  }

  public function setDrop($_drop) {
    $this->drop = $_drop;
  }

  public function setUrlDownload($_url_download) {
    $this->url_download = $_url_download;
  }

  public function setUrlIndex($_url_index) {
    $this->url_index = $_url_index;
  }

  public function setIsTopframe($_is_topframe) {
    $this->is_topframe = $_is_topframe;
  }

}
