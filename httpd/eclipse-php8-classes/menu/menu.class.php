<?php

require_once "/home/data/httpd/eclipse-php8-classes/menu/menuitem.class.php";
/**
 * Menu.class.php.
 *
 * Author:     Denis Roy
 * Date      2004-09-11
 *
 * Description: Functions and modules related to menu objects
 *
 * HISTORY:
 */
class Menu {

  public $MenuItemList = array();

  public function getMenuItemList() {
    return $this->MenuItemList;
  }

  public function setMenuItemList($_MenuItemList) {
    $this->MenuItemList = $_MenuItemList;
  }

  /**
   * Main constructor.
   */
  public function Menu($_Language) {
    $MenuText = "Back to the Portal";
    $MenuItem = new MenuItem($MenuText, "https://dev.eclipse.org/portal/myfoundation/portal/portal.php", "_self");
    $this->MenuItemList[count($this->MenuItemList)] = $MenuItem;

    $MenuText = "Sign-out";
    $MenuItem = new MenuItem($MenuText, "/committers/login/index.php", "_self");
    $this->MenuItemList[count($this->MenuItemList)] = $MenuItem;
  }

  public function addMenuItem($_Text, $_URL, $_Target) {
    // Menu Items must be added at position 1 .. position 0 is dashboard, last position is Signout.
    $MenuItem = new MenuItem($_Text, $_URL, $_Target);

    // Move SignOut Over.
    $this->MenuItemList[count($this->MenuItemList)] = $this->MenuItemList[count($this->MenuItemList) - 1];

    // Add incoming menuitem.
    $this->MenuItemList[count($this->MenuItemList) - 2] = $MenuItem;
  }

  public function getLabel($_Label, $_Language) {

    switch ($_Language) {
      case "en":
        switch ($_Label) {
          case "save":
            return "Save";

          break;
          case "list":
            return "Back to list";

          break;
          case "delete":
            return "Delete";

          break;
        }
        break;

      case "fr":
        switch ($_Label) {
          case "save":
            return "Sauvegarder";

          break;
          case "list":
            return "Retour � la liste";

          break;
          case "delete":
            return "Supprimer";

          break;
        }
        break;
    }
  }

  public function getMenuItemCount() {
    return count($this->MenuItemList);
  }

  public function getMenuItemAt($_Pos) {
    if ($_Pos < $this->getMenuItemCount()) {
      return $this->MenuItemList[$_Pos];
    }
  }

}
