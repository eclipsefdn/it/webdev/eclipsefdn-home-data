<?php

/**
 * Link.class.php.
 *
 * Author:         Denis Roy
 * Date            2004-09-14
 *
 * Description: Functions and modules related to link objects
 *
 * HISTORY:
 *
 */
class Link {

  public $Text   = "";
  public $URL    = "";
  public $Target = "";

  public function getText() {
    return $this->Text;
  }

  public function getURL() {
    return $this->URL;
  }

  public function getTarget() {
    return $this->Target;
  }

  public function setText($_Text) {
    $this->Text = $_Text;
  }

  public function setURL($_URL) {
    $this->URL = $_URL;
  }

  public function setTarget($_Target) {
    $this->Target = $_Target;
  }

  /**
   * Main constructor.
   */
  public function Link($_Text, $_URL, $_Target) {
    $this->setText($_Text);
    $this->setURL($_URL);
    $this->setTarget($_Target);
  }

}
