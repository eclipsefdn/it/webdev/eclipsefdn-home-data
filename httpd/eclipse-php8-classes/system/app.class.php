<?php

/**
 * App.class.php.
 *
 * Author: Denis Roy
 * Date: 2004-08-05
 *
 * Description: Functions and modules related to the application
 *
 */
class App {

  public $APPVERSION         = "1.0";
  public $APPNAME            = "Eclipse.org";
  public $OUR_DOWNLOAD_URL   = "http://download1.eclipse.org";
  public $PUB_DOWNLOAD_URL   = "http://download.eclipse.org";
  public $DOWNLOAD_BASE_PATH = "/home/data/httpd/download.eclipse.org";
  public $PASS_MIN_LENGTH    = 6;

  public function getAppVersion() {
    return $this->APPVERSION;
  }

  public function getAppName() {
    return $this->APPNAME;
  }

  public function runStdWebAppCacheable() {
    session_start();
    header("Cache-control: public");
  }

  public function sendXMLHeader() {
    header("Content-type: text/xml");
  }

  public function getOurDownloadServerUrl() {
    return $this->OUR_DOWNLOAD_URL;
  }

  public function getDownloadBasePath() {
    return $this->DOWNLOAD_BASE_PATH;
  }

  public function getMinPasswordLength() {
    return $this->PASS_MIN_LENGTH;
  }

  public function getPubDownloadServerUrl() {
    return $this->PUB_DOWNLOAD_URL;
  }

  public function getAlphaCode($_NumChars) {
    // Accept: int - number of chars
    // return: string - random alphanumeric code.
    // Generate alpha code.
    $addstring = "";
    for ($i = 1; $i <= $_NumChars; $i++) {
      if (rand(0, 1) == 1) {
        // Generate character.
        $addstring = $addstring . chr(rand(0, 5) + 97);
      }
      else {
        $addstring = $addstring . rand(0, 9);
      }
    }
    return $addstring;
  }

  public function getNumCode($_NumChars) {
    // Accept: int - number of chars
    // return: int - random numeric code.
    // Generate code.
    $addstring = "";
    for ($i = 1; $i <= $_NumChars; $i++) {
      if ($i > 1) {
        // Generate first digit.
        $addstring = $addstring . rand(1, 9);
      }
      else {
        $addstring = $addstring . rand(0, 9);
      }
    }
    return $addstring;
  }

  public function returnQuotedString($_String) {
    // Accept: String - String to be quoted
    // return: string - Quoted String.
    // Replace " with '.
    $_String = str_replace('"', "'", $_String);

    return "\"" . $_String . "\"";
  }

  public function addAndIfNotNull($_String) {
    // Accept: String - String to be AND'ed
    // return: string - AND'ed String.
    if ($_String != "") {
      $_String = $_String . " AND ";
    }
    return $_String;
  }

  public function replaceEnterWithBR($_String) {
    return str_replace("\n", "<br />", $_String);
  }

  public function str_replace_count($find, $replace, $subject, $count) {
    $subjectnew = $subject;
    $pos = strpos($subject, $find);
    if ($pos !== FALSE) {
      while ($pos !== FALSE) {
        $nC = $nC + 1;
        $temp = substr($subjectnew, $pos + strlen($find));
        $subjectnew = substr($subjectnew, 0, $pos) . $replace . $temp;
        if ($nC >= $count) {
          break;
        }
        $pos = strpos($subjectnew, $find);
      } // closes the while loop
    } // closes the if
    return $subjectnew;
  }

  public function getClientOS() {

    $UserAgent = $_SERVER['HTTP_USER_AGENT'];

    $regex_windows = '/([^dar]win[dows]*)[\s]?([0-9a-z]*)[\w\s]?([a-z0-9.]*)/i';
    $regex_mac     = '/(68[k0]{1,3})|(ppc mac os x)|([p\S]{1,5}pc)|(darwin)/i';
    $regex_os2     = '/os\/2|ibm-webexplorer/i';
    $regex_sunos   = '/(sun|i86)[os\s]*([0-9]*)/i';
    $regex_irix    = '/(irix)[\s]*([0-9]*)/i';
    $regex_hpux    = '/(hp-ux)[\s]*([0-9]*)/i';
    $regex_aix     = '/aix([0-9]*)/i';
    $regex_dec     = '/dec|osfl|alphaserver|ultrix|alphastation/i';
    $regex_vms     = '/vax|openvms/i';
    $regex_sco     = '/sco|unix_sv/i';
    $regex_linux   = '/x11|inux/i';
    $regex_bsd     = '/(free)?(bsd)/i';
    $regex_amiga   = '/amiga[os]?/i';

    $regex_x86_64 = "/x86_64/i";

    // Look for Windows Box.
    if (preg_match_all($regex_windows, $UserAgent, $match)) {

      $v = $match[2][count($match[0]) - 1];
      $v2 = $match[3][count($match[0]) - 1];

      // Establish NT 5.1 as Windows XP.
      if (stristr($v, 'NT') && $v2 == 5.1) {
        $v = 'win32';
      }

      // Establish NT 5.0 and Windows 2000 as win2k.
      elseif ($v == '2000') {
        $v = '2k';
      }
      elseif (stristr($v, 'NT') && $v2 == 5.0) {
        $v = 'win32';
      }
      // Establish 9x 4.90 as Windows 98.
      elseif (stristr($v, '9x') && $v2 == 4.9) {
        $v = 'win32';
      }
      // See if we're running windows 3.1.
      elseif ($v . $v2 == '16bit') {
        $v = 'win16';
      }
      // Otherwise display as is (31,95,98,NT,ME,XP)
      else {
        $v .= $v2;
      }
      // Update browser info container array.
      if (empty($v)) {
        $v = 'win32';
      }
      return (strtolower($v));
    }

    // Look for amiga OS.
    elseif (preg_match($regex_amiga, $UserAgent, $match)) {
      if (stristr($UserAgent, 'morphos')) {
        // Checking for MorphOS.
        return ('morphos');
      }
    }
    elseif (stristr($UserAgent, 'mc680x0')) {
      // Checking for MC680x0.
      return ('mc680x0');
    }
    elseif (preg_match('/(AmigaOS [\.1-9]?)/i', $UserAgent, $match)) {
      // Checking for AmigaOS version string.
      return ($match[1]);
    }
    // Look for OS2.
    elseif (preg_match($regex_os2, $UserAgent)) {
      return ('os2');
    }
    // Look for mac
    // sets: platform = mac ; os = 68k or ppc.
    elseif (preg_match($regex_mac, $UserAgent, $match)) {
      $os = !empty($match[1]) ? 'mac68k' : '';
      $os = !empty($match[2]) ? 'macosx' : $os;
      $os = !empty($match[3]) ? 'macppc' : $os;
      $os = !empty($match[4]) ? 'macosx' : $os;
      return ('macosx');
    }
    // Look for *nix boxes
    //  sunos sets: platform = *nix ; os = sun|sun4|sun5|suni86.
    elseif (preg_match($regex_sunos, $UserAgent, $match)) {
      if (!stristr('sun', $match[1])) {
        $match[1] = 'sun' . $match[1];
      }
      return ('solaris');
    }
    // Irix sets: platform = *nix ; os = irix|irix5|irix6|...
    elseif (preg_match($regex_irix, $UserAgent, $match)) {
      return ($match[1] . $match[2]);
    }
    // hp-ux sets: platform = *nix ; os = hpux9|hpux10|...
    elseif (preg_match($regex_hpux, $UserAgent, $match)) {
      $match[1] = str_replace('-', '', $match[1]);
      $match[2] = (int) $match[2];
      return ('hpux');
    }
    // Aix sets: platform = *nix ; os = aix|aix1|aix2|aix3|...
    elseif (preg_match($regex_aix, $UserAgent, $match)) {
      return ('aix');
    }
    // Dec sets: platform = *nix ; os = dec.
    elseif (preg_match($regex_dec, $UserAgent, $match)) {
      return ('dec');
    }
    // Vms sets: platform = *nix ; os = vms.
    elseif (preg_match($regex_vms, $UserAgent, $match)) {
      return ('vms');
    }
    // Dec sets: platform = *nix ; os = dec.
    elseif (preg_match($regex_dec, $UserAgent, $match)) {
      return ('dec');
    }
    // Vms sets: platform = *nix ; os = vms.
    elseif (preg_match($regex_vms, $UserAgent, $match)) {
      return ('vms');
    }
    // Sco sets: platform = *nix ; os = sco.
    elseif (preg_match($regex_sco, $UserAgent, $match)) {
      return ('sco');
    }
    // Unixware sets: platform = *nix ; os = unixware.
    elseif (stristr($UserAgent, 'unix_system_v')) {
      return ('unixware');
    }
    // Mpras sets: platform = *nix ; os = mpras.
    elseif (stristr($UserAgent, 'ncr')) {
      return ('mpras');
    }
    // Reliant sets: platform = *nix ; os = reliant.
    elseif (stristr($UserAgent, 'reliantunix')) {
      return ('reliant');
    }
    // Sinix sets: platform = *nix ; os = sinix.
    elseif (stristr($UserAgent, 'sinix')) {
      return ('sinix');
    }
    // Bsd sets: platform = *nix ; os = bsd|freebsd.
    elseif (preg_match($regex_bsd, $UserAgent, $match)) {
      return ($match[1] . $match[2]);
    }
    // Last one to look for
    //  linux sets: platform = *nix ; os = linux.
    elseif (preg_match($regex_linux, $UserAgent, $match)) {
      if (preg_match($regex_x86_64, $UserAgent, $match)) {
        return "linux-x64";
      }
      else {
        return ('linux');
      }
    }
  }

  /**
   * Fetch the HTTP parameter.
   *
   * @author droy
   * @since version - Oct 19, 2006
   * @param String _param_name name of the HTTP GET/POST parameter
   * @param String _method GET or POST, or the empty string for POST,GET order
   *
   * @return String HTTP GET/POST parameter value, or the empty string
   */
  public function getHTTPParameter($_param_name, $_method = "") {

    $rValue = "";
    $_method = strtoupper($_method);

    // Always fetch the GET VALUE, override with POST unless a GET was specifically requested.
    if (isset($_GET[$_param_name])) {
      $rValue = $_GET[$_param_name];
    }
    if (isset($_POST[$_param_name]) && $_method != "GET") {
      $rValue = $_POST[$_param_name];
    }

    return $rValue;
  }

}