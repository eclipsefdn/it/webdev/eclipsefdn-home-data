<?php

/**
 * *****************************************************************************
 * Copyright (c) 2005, 2017 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * Denis Roy (Eclipse Foundation)- initial API and implementation
 * Christopher Guindon (Eclipse Foundation) - Adding the concept of
 * DBConnectionBase.
 * *****************************************************************************
 */

class DBConnectionBase {

  /**
   * DB Hostname.
   *
   * @var string
   */
  protected $hostname = "";

  /**
   * The MariDB username.
   *
   * @var string
   */
  protected $username = "";

  /**
   * The MariDB password.
   *
   * @var string
   */
  protected $password = "";

  /**
   * The MariDB database.
   *
   * @var string
   */
  protected $database = "";

  /**
   * Mysqli object
   */
  static $Mysqli = array();

  /**
   * Valid caller paths
   *
   * @var array
   */
  protected $validPaths = array();

  /**
   * Constructor
   */
  public function __construct() {
    // Apply settings that applies to all db classes
    $this->setHostname(getenv('MARIADB_HOST'));
    $this->setUsername(getenv('MARIADB_USERNAME'));
    $this->setPassword(getenv('MARIADB_PASSWORD'));
    $this->setValidPaths("/localsite");
    $this->setValidPaths("/var/www/html");
    $this->setValidPaths("/home/data/httpd");
  }

  /**
   * Connect to DB
   *
   * @return \mysqli $Mysqli
   */
  public function connect() {

    $this->validateCaller();
    $Mysqli = mysqli_connect($this->getHostname(), $this->getUsername(), $this->getPassword());

    if (!$Mysqli) {
      echo ("<p>Unable to connect to the database server at this time (" . get_class($this) . ").</p>");
      // header('HTTP/1.1 500 Internal Server Error'); We prefer to see the error in dev mode.
      die();
    }

    $db_selected = mysqli_select_db($Mysqli, $this->getDatabase());
    if (!$db_selected) {
      die("Error database not found:" . $this->getDatabase() . ": " . mysqli_error($Mysqli));
    }

    self::setMysqli($Mysqli);
    return $Mysqli;
  }

  /**
   * Close DB connection
   */
  public function disconnect() {
    $Mysqli = self::getMysqli();
    if (!empty($Mysqli) && $Mysqli instanceof mysqli) {
      mysqli_close($Mysqli);
      self::setMysqli(NULL);
    }
  }

  /**
   * Get link identifier
   *
   * @return mysqli|null
   */
  static public function getMysqli() {
    if (!empty(self::$Mysqli[get_called_class()])) {
      return self::$Mysqli[get_called_class()];
    }
    return NULL;
  }


  /**
   * Set link identifier
   *
   * @return string
   */
  static protected function setMysqli($link_identifier = "") {
    self::$Mysqli[get_called_class()] = $link_identifier;
  }

  /**
   * Validate database caller
   *
   * @return boolean or DIE!!
   */
  public function validateCaller() {
    $debug_backtrace = debug_backtrace();
    $valid_caller = FALSE;
    $valid_path = $this->getValidPaths();

    foreach ($debug_backtrace as $trace) {
      if (empty($trace['file'])) {
        continue;
      }

      $caller = $trace['file'];
      foreach ($valid_path as $path) {
        if (strstr($caller, $path)) {
          $valid_caller = TRUE;
          break;
        }
      }

      // stop the loop after finding a valid caller
      if ($valid_caller) {
        break;
      }
    }

    if (!$valid_caller) {
      echo "Execution from Invalid Path (" . get_class($this) . "). This attempt has been logged. Please contact webmaster@eclipse.org";
      exit();
    }

    return TRUE;
  }

  /**
   * Get database
   */
  protected function getDatabase() {
    return $this->database;
  }

  /**
   * Set database
   *
   * @param string $database
   */
  protected function setDatabase($database = "") {
    return $this->database = $database;
  }

  /**
   * Get password
   */
  protected function getPassword() {
    return $this->password;
  }

  /**
   * Set password
   *
   * @param string $password
   */
  protected function setPassword($password = "") {
    $this->password = $password;
  }

  /**
   * Get hostname
   *
   * @return string $hostname
   */
  protected function getHostname() {
    return $this->hostname;
  }

  /**
   * Set hostname
   *
   * @param string $hostname
   */
  protected function setHostname($hostname = "") {
    $this->hostname = $hostname;
  }

  /**
   * Get username
   */
  protected function getUsername() {
    return $this->username;
  }

  /**
   * Set username
   *
   * @param string $user
   */
  protected function setUsername($user = "") {
    $this->username = $user;
  }


  /**
   * Set validPaths
   */
  protected function getValidPaths() {
    return $this->validPaths;
  }

  /**
   * Set validPaths
   *
   * @param string $path
   */
  protected function setValidPaths($path = "") {
    $this->validPaths[] = $path;
  }
}
