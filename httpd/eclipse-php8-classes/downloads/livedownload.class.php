<?php

require_once "/home/data/httpd/eclipse-php8-classes/system/dbconnection.class.php";
require_once "/home/data/httpd/eclipse-php8-classes/system/app.class.php";

/**
 * Livedownload.class.php.
 *
 * Author:       Denis Roy
 * Date:         2005-06-1
 *
 * Description:  get live download count according to label
 */
class LiveDownload {

  public function selectDownloadCount($_label) {

    // Connect to database.
    $dbc = new DBConnection();
    $dbh = $dbc->connect();

    $App = new App();

    $sql = "SELECT dl_count FROM downloads_31 WHERE file = " . $App->returnQuotedString($_label);
    $rs = mysqli_query($dbh, $sql);

    $myrow = mysqli_fetch_assoc($rs);
    $dl_count = $myrow['dl_count'];

    $dbc->disconnect();
    unset($dbc);
    return $dl_count;
  }
}
