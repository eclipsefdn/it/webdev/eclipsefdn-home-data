<?php

require_once "/home/data/httpd/eclipse-php8-classes/system/dbconnection.class.php";

/**
 * Drop.class.php.
 *
 * Author:   Denis Roy
 * Date:    2004-11-16
 *
 * Description: Functions and modules related to drop objects
 *
 * HISTORY:
 */
class Drop {

  public $drop_id = "";
  public $project_id = "";
  public $rsync_stanza = "";
  public $description = "";
  public $is_active = 0;
  public $our_path = "";
  public $size = 0;
  public $size_unit = "";
  public $index_url = "";

  public function getDropID() {
    return $this->drop_id;
  }

  public function getProjectID() {
    return $this->project_id;
  }

  public function getRsyncStanza() {
    return $this->rsync_stanza;
  }

  public function getDescription() {
    return $this->description;
  }

  public function getIsActive() {
    return $this->is_active;
  }

  public function getOurPath() {
    return $this->our_path;
  }

  public function getSize() {
    return $this->size;
  }

  public function getSizeUnit() {
    return $this->size_unit;
  }

  public function setDropID($_drop_id) {
    $this->drop_id = $_drop_id;
  }

  public function setProjectID($_project_id) {
    $this->project_id = $_project_id;
  }

  public function setRsyncStanza($_rsync_stanza) {
    $this->rsync_stanza = $_rsync_stanza;
  }

  public function setDescription($_description) {
    $this->description = $_description;
  }

  public function setIsActive($_is_active) {
    $this->is_active = $_is_active;
  }

  public function setOurPath($_our_path) {
    $this->our_path = $_our_path;
  }

  public function setSize($_size) {
    $this->size = $_size;
  }

  public function setSizeUnit($_size_unit) {
    $this->size_unit = $_size_unit;
  }

}
